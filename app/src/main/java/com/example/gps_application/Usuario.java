package com.example.gps_application;


//Classe de nome de usuário, para informar no cadastro, com getters e setters.

public class Usuario {

    private String userId;
    private String userName;


    public Usuario(String userId, String userName) {
        this.userId = userId;
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
