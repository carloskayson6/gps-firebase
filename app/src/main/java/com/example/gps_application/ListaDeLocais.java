package com.example.gps_application;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ListaDeLocais extends AppCompatActivity {

    private ListView listView;
    DatabaseReference databaseReference;
    List<Local> localList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_de_locais);

        listView = findViewById(R.id.lista);

        databaseReference = FirebaseDatabase.getInstance().getReference("Locais");
        localList = new ArrayList<>();
    }

    @Override
    protected void onStart() {
        super.onStart();
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
               for(DataSnapshot localSnapshot : dataSnapshot.getChildren()){
                   Local local = localSnapshot.getValue(Local.class);
                   localList.add(local);
               }
               LocalAdapter localAdapter = new LocalAdapter(ListaDeLocais.this, localList);
               listView.setAdapter(localAdapter);
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
