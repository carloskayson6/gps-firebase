package com.example.gps_application;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Registro extends AppCompatActivity {

    //activity de registro do usuário, onde ele informa nome, email e senha para poder se registrar

    private Button register;
    EditText nome, senha, email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        register = (Button) findViewById(R.id.btnRegister);
        email = (EditText) findViewById(R.id.editEmailRegistro);
        senha = (EditText) findViewById(R.id.editSenhaRegistro);
        nome = (EditText) findViewById(R.id.editNomeRegistro);

        register.setOnClickListener(new View.OnClickListener() {//botão registrar-se só irá cadastrar
            @Override                                           //se o email e a senha estiverem preenchidos e
            public void onClick(View view) {                    //com o ideal formato.
                Intent i = new Intent(Registro.this, MainActivity.class);//ex@gmail.com
                                                                        //123456
                if(email != null && senha != null){
                    createUser();    //caso os seus parametros email e senha estiverem devidamente preenchidos, o app
                    startActivity(i);//cria um usuário autenticado no firebase e manda para a tela inicial
                }else{
                    startActivity(i);//se email ou senha não forem preenchidos, o app manda direto pra tela inicial do app
                }   //com a mensagem: "Senha e Email devem ser preenchidos"
            }
        });
    }

    private void createUser() {
        String emailRegister = email.getText().toString();
        String senhaRegister = senha.getText().toString();

        Usuario usuario = new Usuario(emailRegister, senhaRegister);



        if(emailRegister == null || emailRegister.isEmpty() || senhaRegister == null || senhaRegister.isEmpty()){
            Toast.makeText(this, "Senha e Email devem ser preenchidos", Toast.LENGTH_SHORT).show();
            return;
        }

        FirebaseAuth.getInstance().createUserWithEmailAndPassword(emailRegister, senhaRegister)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Log.i("Teste", task.getResult().getUser().getUid());
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.i("Teste", e.getMessage());
            }
        });



    }
}
