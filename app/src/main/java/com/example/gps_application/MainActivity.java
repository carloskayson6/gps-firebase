package com.example.gps_application;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

    EditText senha, email;
    Button btn;             //variáveis para a tela de login, onde se conecta
    TextView txt;           //ao firebase caso o usuario seja cadastrado.
    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = (Button) findViewById(R.id.buttonLogin);
        txt = (TextView) findViewById(R.id.txtCadastro);        //iniciação das variáveis
        senha = (EditText) findViewById(R.id.editSenhaLogin);
        email = (EditText) findViewById(R.id.editEmailLogin);

        txt.setOnClickListener(new View.OnClickListener() {//caso não se tenha um cadastro, clicando em
            @Override                                       //"Nao tem uma conta? Clique aqui!"
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, Registro.class);
                startActivity(i);                           //inicia a activity que se tem a intenção de chegar
            }
        });

        btn.setOnClickListener(new View.OnClickListener() { //botão de login que nos leva para a tela
            @Override                                       //de opções do aplicativo, onde temos "Maps" e "Lista de Locais"
            public void onClick(View view) {
                String _email = email.getText().toString();
                String _senha = senha.getText().toString();

                if(email == null || senha == null){         //caso e Email ou a senha estiverem incorretos, o aplicativo fecha
                    Toast.makeText(MainActivity.this, "Email ou Senha incorreto", Toast.LENGTH_SHORT).show();
                    return;
                }

                FirebaseAuth.getInstance().signInWithEmailAndPassword(_email, _senha)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(task.isSuccessful());
                                Log.i("Teste", task.getResult().getUser().getUid());
                            }
                        });
                Intent i = new Intent(MainActivity.this, PosLogin.class);
                startActivity(i); //inicia a Activity PosLogin
                //
            }
        });
    }


}

