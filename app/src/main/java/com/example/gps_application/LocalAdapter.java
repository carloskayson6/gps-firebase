package com.example.gps_application;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class LocalAdapter extends ArrayAdapter<Local> {

    //classe com Array adapter que abriga uma listview para que se consiga obter itens listados do firebase

    private Activity context;
    private List<Local> localList;

    public LocalAdapter(Activity context, List<Local> localList){
        super(context, R.layout.list_view, localList);
        this.context = context;
        this.localList = localList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @Nullable
            ViewGroup parent){
        LayoutInflater inflater = context.getLayoutInflater();
        View listView = inflater.inflate(R.layout.list_view, null, true);

        TextView latitude = (TextView) listView.findViewById(R.id.latid);
        TextView longitude = (TextView) listView.findViewById(R.id.longit);


        Local local = localList.get(position);

        latitude.setText(String.valueOf(local.getLatitude()));
        longitude.setText(String.valueOf(local.getLongitude()));


        return listView;
    }

}


