package com.example.gps_application;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;

public class PosLogin extends AppCompatActivity {

    //nessa activity temos apenas botões,
    //sendo 3.
    //(1). Botão toolbar que contém o logout
    //(2). Botão de maps que mostra a localização atual e as antigas do usuário
    //(3). Botão que mostra a lista de locais de um determinado usuário.

    private Button btnMaps, btnList;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_bar, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.logout){
            FirebaseAuth.getInstance().signOut();
            finish();
        }
    return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pos_login);

        btnMaps = (Button) findViewById(R.id.maps);
        btnList = (Button) findViewById(R.id.list);

        btnMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(PosLogin.this, MapsActivity.class);
                startActivity(i);
            }
        });

        btnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(PosLogin.this, ListaDeLocais.class);
                startActivity(i);

            }
        });



    }
}
